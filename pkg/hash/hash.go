package hash

import (
	pb "gitlab.com/danialmaki.dm/2_go_layout/pkg/autogenerated"
)

type Hash interface {
	GetRecord(reqStruct *pb.SubmitRequest, nonce int64) (string, error)
	GetSha256(str string) (string, error)
}
